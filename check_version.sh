file_list=$1


if [ $# -eq 0 ]
then
        echo "USAGE: sh check_version.sh SERVER_LIST"
        echo "EXAMPLE: sh check_version.sh server.txt "
else
	IFS=$'\n'

        for i in $(cat $file_list | sed -n 'p' | tr ',' ' ' )
        do
		server=$(echo $i | awk '{print $1}')
                if var=$(knife node list |grep -m 1 -i $server)
                then
		knife node show $var -a chef_packages.chef.version | tr "\n" " " | awk '{print $1 $3}' | sed 's/\:/ /g'                
                sleep 2
                else
                echo "server $server cannot be seen in knife node list"
                fi
        done
fi
