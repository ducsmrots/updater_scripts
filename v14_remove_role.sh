#!bin/bash

##VARIABLES##

file_list=$1
failed_upgrade_ver=$2
date=$(date +%m%d%Y%H%M%S)
knife_dir="$HOME/knife_logs"

if [ $# -eq 0 ]
then
        echo "USAGE: sh remove_role.sh SERVER_LIST ROLE_NAME"
        echo "EXAMPLE: sh remove_role.sh server.txt centos7"
else
        if [ ! -d $knife_dir ]
        then
                mkdir $knife_dir
        else
                echo "You can find the logs at $knife_dir"
        fi

        success_logs=''$knife_dir'/'$date.success_remove_log''
        error_logs=''$knife_dir'/'$date.error_remove_log

	for j in $(cat $failed_upgrade_ver)
	do
	IFS=$'\n'

        	for i in $(cat $file_list | sed -n 'p' | tr ',' ' ' |grep $j)
        	do
                	server=$(echo $i | awk '{print $1}')
                	new_role=$(echo $i | awk '{print $2}')
                	old_role=$(echo $i | awk '{print $3}')
                	ciid=$(echo $i | awk '{print $4}')
			if var=$(knife node list |grep -m 1 -i $server)
                	then
                	echo "Starting to remove role: $old_role on $var"
                	echo "You got 3 seconds to cancel......"
                	sleep 3
                	knife node run_list remove $var 'role['$new_role']' >> $success_logs
			knife node run_list add $var 'role['$old_role']' >> $success_logs
                	echo "Finished removing role"
                	else
                	echo "server $i cannot be seen in knife node list" >> $error_logs
                	fi
        	done
	done
fi
