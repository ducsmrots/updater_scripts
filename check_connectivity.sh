#!/bin/bash

file_list=$1

if [ $# -eq 0 ]
then
        echo "USAGE: sh check_connectivity.sh SERVER_LIST_CSV"
        echo "EXAMPLE: sh check_connectivity.sh server.csv "
else
 		
	IFS=$'\n'
	
	echo ""	
	echo "Please make sure that the CSV file is in the sample format below: "
	echo "apsrt4140,chef_client_v13_8_5,chef_client_v13_6_4,CI##########"
	echo ""
	echo "You have 10 seconds to cancel......"
	echo ""
	sleep 5

	for i in $(cat $file_list | sed -n 'p' | tr ',' ' ' )

	do
		server=$(echo $i | awk '{print $1}')
        	if ping -c4 $server > /dev/null 2>&1
        	then
                	echo $server : reachable

        	else
                	echo $server : not reachable

        	fi
	done
fi
