#!/bin/bash

##VARIABLES##
file_list=$1

echo "\n"

for i in $(cat $file_list)

do

        echo "\t\t$i"
        echo ==============================================================
        ssh -qqq root@$i "
	echo "CHECKING RPM STATUS: "
        rpm -qa chef
        sleep 3
	echo "CHECKING disk space for opt: "
        df -h /opt
        "
        echo ==============================================================
done
