#!bin/bash

##VARIABLES##

file_list=$1
#updater_role=$2
date=$(date +%m%d%Y%H%M%S)
knife_dir="$HOME/knife_logs"

if [ $# -eq 0 ] 
then
        echo "USAGE: sh new_add_role.sh SERVER_LIST.CSV" 
	echo "EXAMPLE: sh new_add_role.sh server_list.csv"
else
	if [ ! -d $knife_dir ]
	then
		mkdir $knife_dir
	else
		echo "You can find the logs at $knife_dir"
	fi
	
	success_logs=''$knife_dir'/'$date.success_add_log''
	error_logs=''$knife_dir'/'$date.error_add_log

	IFS=$'\n'

        for i in $(cat $file_list | sed -n 'p' | tr ',' ' ')
        do
		server=$(echo $i | awk '{print $1}')
		new_role=$(echo $i | awk '{print $2}')
		old_role=$(echo $i | awk '{print $3}')
		ciid=$(echo $i | awk '{print $4}')
		if var=$(knife node list |grep -m 1 -i $server)
		then
		echo "Starting to delete role: $old_role on $var"
		echo "Starting to add role: $new_role on $var"
		echo "You got 5 seconds to cancel......"
		sleep 5
		knife node run_list remove $var 'role['$old_role']' >> $success_logs
		knife node run_list add $var 'role['$new_role']' >> $success_logs
		echo "Finished removing $old_role"
		echo "Finished adding $new_role"
		else
		echo "server $server cannot be seen in knife node list" >> $error_logs
		fi
        done
fi
